import {useState, forwardRef, useImperativeHandle} from "react";

import {theme} from "../constants/theme";
import {Icon} from "./Icon";

export const NotificationModal = forwardRef(({}, ref) => {
    const [isOpen, setIsOpen] = useState(false)
    const [message, setMessage] = useState(false)
    const [isValid, setIsValid] = useState(false)

    const open = (messageParam, isValidParam) => {
        setMessage(messageParam)
        setIsValid(isValidParam)
        setIsOpen(true)
    }

    const close = () => {
        setIsOpen(false)
    }

    useImperativeHandle(ref, () => {
        return {
            open: (messageParam, isValidParam) => open(messageParam, isValidParam),
            close: () => close()
        }
    })

    if (isOpen) {
        return (
            <div
                style={{
                    position: 'fixed',
                    top: theme.sizes.s,
                    width: '100%',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    zIndex: 2000,
                }}
            >
                <div style={{
                    paddingRight: theme.sizes.m,
                    paddingLeft: theme.sizes.m,
                    paddingTop: theme.sizes.s,
                    paddingBottom: theme.sizes.s,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: theme.sizes.l,
                    boxShadow: '0px 10px 15px rgba(206, 206, 206, 0.4)',
                    backgroundColor: isValid ? theme.palette.green.light1 : theme.palette.red.light1,
                }}>
                    <Icon
                        name={isValid ? 'checkmark' : 'xmark'}
                        size={theme.sizes.s}
                        style={{
                            marginRight: theme.sizes.s,
                        }}
                        color={isValid ? theme.colors.success : theme.colors.danger}
                    />
                    <div
                        style={{
                            ...theme.textSizes.body2,
                            color: isValid ? theme.colors.success : theme.colors.danger
                        }}
                    >
                        {message}
                    </div>
                </div>
            </div>
        )
    }
})