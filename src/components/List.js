import {theme} from "../constants/theme";
import {Label} from "./Label";
import {Icon} from "./Icon";
import {Loader} from "./Loader";
import {SearchBar} from "./SearchBar";
import {Dot} from "./Dot";

export const List = ({
                         items,
                         rows,
                         searchBar,
                         onSearch,
                         searchValue,
                         onPress,
                         labels,
                         onLoadMorePress,
                         isLoading,
                         selectedItemId
                     }) => {
    return (
        <div style={{
            width: '100%'
        }}>
            {
                searchBar &&
                <SearchBar
                    placeholder="Search..."
                    onChange={onSearch}
                    value={searchValue}
                    style={{
                        position: 'sticky',
                        top: 55,
                    }}
                />
            }
            {
                items.map((item) =>
                    <div
                        key={item.id}
                        style={{
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            paddingLeft: theme.sizes.m,
                            paddingRight: theme.sizes.m,
                            height: theme.sizes.xxl,
                            borderBottom: 1,
                            borderBottomStyle: 'solid',
                            borderBottomColor: theme.colors.disabled,
                            cursor: 'pointer',
                        }}
                        onClick={() => onPress(item.id)}
                    >
                        <div style={{
                            color: selectedItemId === item.id ? theme.colors.primary : theme.colors.text,
                            ...theme.textSizes.body1,
                            textOverflow: 'ellipsis',
                            whiteSpace: 'nowrap',
                            overflow: 'hidden',
                            maxHeight: theme.sizes.xxl + 4,
                            maxWidth: '55%',
                        }}>
                            {item.name}
                        </div>
                        <div style={{display: 'flex', justifyContent: 'end'}}>
                            {
                                labels.map((label, index) => (
                                    <div key={index}>
                                        {
                                            !!label.formatFunction(item[label.field]) &&
                                            <Dot
                                                color={label.formatFunction(item[label.field])}
                                                style={{
                                                    marginLeft: theme.sizes.s,
                                                }}
                                            />
                                        }
                                    </div>
                                ))
                            }
                        </div>
                    </div>
                )
            }
            {
                items.length !== rows && !isLoading &&
                <div style={{
                    display: "flex",
                    justifyContent: 'center',
                    paddingTop: theme.sizes.s,
                    paddingBottom: theme.sizes.s,
                    cursor: 'pointer'
                }}
                     onClick={() => onLoadMorePress()}
                >
                    <Icon name="plus" size={theme.sizes.l} color={theme.colors.primary}/>
                </div>
            }
            {isLoading &&
                <div style={{
                    paddingTop: theme.sizes.s,
                    paddingBottom: theme.sizes.s,
                }}>
                    <Loader color={theme.colors.primary} size="xxs"/>
                </div>
            }
        </div>
    )
}