import IcoMoon from "react-icomoon";
import iconSet from "../assets/icons/selection.json";

export const Icon = (props) => <IcoMoon iconSet={iconSet} icon={props.name} {...props} />;
