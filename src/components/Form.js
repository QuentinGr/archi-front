import {cloneElement, Children, useState, useEffect, useRef} from "react";

import {Controller} from "react-hook-form";

import {validationMessages} from "../constants/validations";
import {theme} from "../constants/theme";

export const FormGroup = ({control, errors, children}) => {
    return (
        Children.map(children, (child) => child ? cloneElement(child, {control, errors}) : <></>)
    )
}

export const FormController = ({
                                   label,
                                   hideLabel = false,
                                   control,
                                   rules,
                                   name,
                                   errors,
                                   description,
                                   children,
                                   ...props
                               }) => {
    const [color, setColor] = useState(theme.colors.text)
    const [isLoaded, setIsLoaded] = useState(false)

    const onFocus = () => {
        if (!errors[name]) {
            setColor(theme.colors.primary)
        }
    }

    const onBlur = () => {
        if (!errors[name]) {
            setColor(theme.colors.text)
        }
    }

    useEffect(() => {
        if (errors[name]) {
            setColor(theme.colors.danger)
        } else {
            if (isLoaded) {
                setColor(theme.colors.primary)
            } else {
                setIsLoaded(true)
            }
        }
    }, [errors[name]])


    return (
        <div style={{zIndex: 2, marginBottom: theme.sizes.s, ...props.style}}>
            {!hideLabel &&
                <div style={{
                    ...theme.textSizes.body2,
                    marginBottom: theme.sizes.xs,
                    color: theme.colors.text
                }}>{label}</div>
            }
            <Controller
                control={control}
                rules={rules}
                name={name}
                render={(field) => cloneElement(children, {...field, onBlur, onInputFocus: onFocus, color})}
            />
            <ErrorHandler
                errors={errors}
                name={name}
            />
            <div
                style={{
                    paddingTop: theme.sizes.xxs,
                }}
            >
                {
                    typeof description === 'string' &&
                    <div style={{color: theme.colors.disabled}}>
                        {description}
                    </div>
                }
                {
                    typeof description !== 'string' &&
                    <div>
                        {description}
                    </div>
                }

            </div>
        </div>
    )
}

export const ErrorHandler = ({errors, name}) => {
    if (errors[name]) {
        return (
            <div
                style={{
                    marginTop: theme.sizes.xs,
                    color: theme.colors.danger,
                    ...theme.textSizes.body1
                }}
            >
                {validationMessages[errors[name].type]}
            </div>
        )
    }
}