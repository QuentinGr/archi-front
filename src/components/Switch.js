import Switch from "react-switch";
import {theme} from "../constants/theme";

export const SwitchF = ({value, onChange}) => {
    return(
        <Switch
            checked={value}
            onChange={onChange}
            offColor={theme.colors.disabled}
            onColor={theme.colors.primary}
            uncheckedIcon={false}
            checkedIcon={false}
            height={theme.sizes.m}
            width={theme.sizes.xl + theme.sizes.xxs}
        />
    )
}