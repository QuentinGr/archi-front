import {ButtonContainer} from "./Button";
import {theme} from "../constants/theme";
import {Icon} from "./Icon";

export const Label = ({
                          title,
                          color = 'primary',
                          size = 'm',
                          shadowSize,
                          filled = true,
                          border = true,
                          fullWidth = false,
                          icon,
                          style,
                          onPress = () => {
                          }
                      }) => {
    const paddingHorizontal = {
        xs: theme.sizes.xxs,
        s: theme.sizes.xxs,
        m: theme.sizes.xs,
        l: theme.sizes.xs,
        xl: theme.sizes.s
    }

    const marginIcon = {
        xs: theme.sizes.xxs,
        s: theme.sizes.xxs,
        m: theme.sizes.xs,
        l: theme.sizes.xs,
        xl: theme.sizes.s,
    }

    return (
        <ButtonContainer
            color={color}
            shadowSize={shadowSize}
            filled={filled}
            border={border}
            fullWidth={fullWidth}
            onPress={onPress}
            style={{
                paddingTop: theme.sizes.xxs,
                paddingBottom: theme.sizes.xxs,
                paddingLeft: paddingHorizontal[size],
                paddingRight: paddingHorizontal[size],
                borderRadius: theme.sizes.xxs,
                ...style
            }}
        >
            {icon &&
                <div style={{marginRight: marginIcon[size]}}>
                    <Icon
                        name={icon}
                        size={theme.sizes[size]}
                        color={filled ? theme.colors.background : theme.colors[color]}
                    />
                </div>
            }
            <div style={{
                fontSize: theme.sizes[size],
                fontFamily: theme.fonts.bold,
                color: filled ? theme.colors.background : theme.colors[color],
                width: fullWidth ? '100%' : 'fit-content',
                textAlign: 'center',
            }}>{title}</div>
        </ButtonContainer>
    )
}