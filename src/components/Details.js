import {forwardRef, useRef, useImperativeHandle} from "react";
import {OverModal} from "./Modal";
import {theme} from "../constants/theme";


const PAGE_SIZE = 10

export const Details = forwardRef(({place, isMobile}, ref) => {

    const modalRef = useRef(null)

    useImperativeHandle(ref, () => {
        return {
            open: () => modalRef.current.open(),
            close: () => modalRef.current.close()
        }
    })

    return (
        <OverModal
            ref={modalRef}
            size={isMobile ? "xs" : "xl"}
            footer={false}
            title="Details"
            style={{
                alignItems: "center",
                justifyContent: "center",
            }}
        >
            <div style={{
                ...theme.textSizes.subtitle1,
                color: theme.colors.text,
                marginBottom: theme.sizes.s,
            }}>
                Description
            </div>
            <div style={{
                ...theme.textSizes.body2,
                color: theme.colors.text,
            }}>
                {place?.details?.commercial_infos?.description}
            </div>
            <div style={{
                ...theme.textSizes.subtitle1,
                color: theme.colors.text,
                marginBottom: theme.sizes.s,
                marginTop: theme.sizes.l,
            }}>
                Coordinates
            </div>
            <div style={{
                ...theme.textSizes.body2,
                color: theme.colors.text,
            }}>
                Phone : {place?.details?.commercial_infos?.phone}
            </div>
            <div style={{
                ...theme.textSizes.body2,
                color: theme.colors.text,
            }}>
                Website : {place?.details?.commercial_infos?.web_site}
            </div>
        </OverModal>
    )
})