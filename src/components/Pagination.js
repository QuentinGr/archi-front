import {useEffect, useState} from "react";
import {theme} from "../constants/theme";
import {Icon} from "./Icon";

export const Pagination = ({page = 1, rows, pageSize, onPageChange, style}) => {
    const [pages, setPages] = useState([])

    useEffect(() => {
        setPages(c =>
            Array(Math.ceil(rows / pageSize))
                .fill(0, 0, Math.ceil(rows / pageSize))
                .map((item, index) => index + 1)
        )
    }, [rows, pageSize])
    return (
        <div
            style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                cursor: 'pointer',
                ...style
            }}
        >
            {
                page > 1 &&
                <Icon
                    name="chevron-left"
                    size={theme.sizes.m}
                    color={theme.colors.primary}
                    onClick={() => onPageChange(page - 1)}
                />
            }
            {
                pages.slice(page === 1 ? page-1 : page-2, page === 1 ? page+2 : page+1).map((pageLoop, index) =>
                    <div
                        key={index}
                        style={{
                            ...theme.textSizes.body2,
                            color: pageLoop === page ? theme.colors.primary : theme.colors.text,
                            marginLeft: theme.sizes.s,
                            marginRight: theme.sizes.s,
                        }}
                        onClick={() => onPageChange(pageLoop)}
                    >
                        {pageLoop}
                    </div>
                )
            }
            {
                page < pages.length &&
                <Icon
                    name="chevron-right"
                    size={theme.sizes.m}
                    color={theme.colors.primary}
                    onClick={() => onPageChange(page + 1)}
                />
            }
        </div>
    )
}