import {SwitchF} from "../Switch";

export const SwitchFilter = ({name, filters, setFilters}) => {

    const onFilterChange = (checked) => {
        setFilters(f => {
            f[name] = checked
            return {...f}
        })
    }

    return(
        <div>
            <SwitchF
                value={filters[name]}
                onChange={onFilterChange}
            />
        </div>
    )
}