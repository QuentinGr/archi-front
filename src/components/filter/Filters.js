import {useRef, forwardRef, useImperativeHandle} from "react";
import {OverModal} from "../Modal";

import {theme} from "../../constants/theme";
import {SwitchFilter} from "./SwitchFilter";

export const Filters =  forwardRef(({filters, setFilters, filterParams}, ref) => {
    const modalRef = useRef(null)

    useImperativeHandle(ref, () => {
        return {
            open: () => modalRef.current.open(),
            close: () => modalRef.current.close()
        }
    })

    return(
        <OverModal
            title="Filtres"
            ref={modalRef}
            footer={false}
        >
            <div>
                {filterParams.map((item, index) =>
                    <div style={{marginBottom: theme.sizes.l}} key={index}>
                        <div style={{
                            ...theme.textSizes.body2,
                            color: theme.colors.text,
                            marginBottom: theme.sizes.s,
                        }}>{item.title}</div>
                        {
                            item.type === 'switch' &&
                            <SwitchFilter
                                name={item.name}
                                filters={filters}
                                setFilters={setFilters}
                            />
                        }
                    </div>
                )}
            </div>
        </OverModal>
    )
})