import {forwardRef, useImperativeHandle, useRef} from "react";

import {FormController} from "../Form";

import {theme} from "../../constants/theme";
import {Icon} from "../Icon";

export const TextInputForm = forwardRef(({icon, ...props}, ref) => {
    const inputRef = useRef()

    useImperativeHandle(ref, () => {
        return {
            inputRef: inputRef.current.inputRef.current,
        }
    })

    return (
        <>
            <FormController
                {...props}
            >
                <TextInputFormComponent
                    ref={inputRef}
                    icon={icon}
                    {...props}
                />
            </FormController>
        </>
    )
})

export const TextInputFormComponent = forwardRef(({icon, onIconPress, onFocus, ...props}, ref) => {
    const inputRef = useRef(null)

    const onChange = (data) => {
        props.field.onChange(data)
        if (props.onChangeFunction) {
            props.onChangeFunction(data)
        }
    }

    useImperativeHandle(ref, () => {
        return {
            inputRef: inputRef,
        }
    })


    return (
        <div
            style={{
                borderWidth: theme.borderSizes.s,
                borderRadius: theme.sizes.s,
                borderColor: props.color,
                borderStyle: 'solid',
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'center',
                width: '100%',
            }}
        >
            <input
                ref={inputRef}
                onBlur={props.onBlur}
                onChange={onChange}
                onFocus={props.onInputFocus}
                value={props.field.value}
                placeholder={props.placeholder}
                style={{
                    color: theme.colors.text,
                    width: '92%',
                    paddingTop: theme.sizes.s,
                    paddingBottom: theme.sizes.s,
                    marginLeft: theme.sizes.m,
                    marginRight: theme.sizes.m,
                    border: 'none',
                    boxShadow: 'none',
                    outline: 'none',
                }}
                color={theme.colors.text}
                {...props}
            />
            {icon &&
                <div>
                    <div onClick={onIconPress} style={{cursor: 'pointer', display: 'flex', alignItems: 'center',  marginRight: theme.sizes.m,}}>
                        <Icon
                            name={icon}
                            size={theme.sizes.m}
                            color={props.color}
                        />
                    </div>
                </div>
            }
        </div>
    )
})