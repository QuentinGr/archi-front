import {useState, forwardRef, useImperativeHandle, useRef} from "react";
import {Icon} from "./Icon";

import {theme} from "../constants/theme";
import {Button} from "./Button";

const SIZES = {
    xs: 300,
    s: 400,
    m: 600,
    l: 800,
    xl: 1000,
}

export const OverModal = forwardRef(({children, title, size = "m", footer = true, onSubmit, style, loading}, ref) => {
    const [isVisible, setIsVisible] = useState(false)


    useImperativeHandle(ref, () => {
        return {
            open: () => open(),
            close: () => close()
        }
    })

    const open = () => {
        setIsVisible(true)
    }

    const close = async () => {
        setIsVisible(false)
    }

    if (isVisible) {
        return (
            <div
                style={{
                    position: 'absolute',
                    left: 0,
                    right: 0,
                    bottom: 0,
                    top: 0,
                    zIndex: 10,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            >
                <div
                    style={{
                        background: "rgba(57, 57, 57, 0.24)",
                        position: 'absolute',
                        left: -200,
                        right: -200,
                        bottom: -200,
                        top: -200,
                    }}
                    onClick={() => setIsVisible(false)}
                />
                <div style={{
                    position: 'relative',
                    zIndex: 4,
                    backgroundColor: theme.colors.background,
                    width: SIZES[size],
                    height: '80%',
                    margin: theme.sizes.m,
                    padding: theme.sizes.l,
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "space-between",
                    boxShadow: '0px 10px 20px rgba(206, 206, 206, 0.4)',
                    borderRadius: theme.sizes.s,
                }}>
                    <div style={{
                        display: 'flex',
                        alignItems: "center",
                        justifyContent: 'space-between'
                    }}>
                        <Icon
                            name="xmark"
                            size={theme.sizes.l}
                            color={theme.colors.primary}
                            onClick={() => setIsVisible(false)}
                            style={{
                                cursor: 'pointer',
                            }}
                        />
                        <div style={{
                            ...theme.textSizes.subtitle1,
                            color: theme.colors.text,
                        }}>
                            {title}
                        </div>
                        <div></div>
                    </div>
                    <div style={{
                        width: '100%',
                        height: '90%',
                        overflow: "auto",
                        paddingRight: theme.sizes.m,
                        marginTop: theme.sizes.m,
                        marginBottom: theme.sizes.m,
                        ...style
                    }}>
                        {children}
                    </div>
                    {
                        footer &&
                        <div style={{
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: "space-between"
                        }}>
                            <Button
                                title="Cancel"
                                color="secondary"
                                onPress={() => setIsVisible(false)}
                            />
                            <Button
                                title="Submit"
                                loading={loading}
                                onPress={() => onSubmit()}
                            />
                        </div>
                    }
                </div>
            </div>
        )
    }
})