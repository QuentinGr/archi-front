import {theme} from "../constants/theme";
import {CircleButton} from "./Button";

export const Card = ({
                         title,
                         button = false,
                         icon,
                         isButtonLoading = false,
                         onPress = () => {
                         },
                         children,
                         asideTitle,
                         asideButton,
                         style
                     }) => {
    return (
        <div
            style={{
                width: '100%',
                height: '100%',
                boxShadow: '0px 10px 20px rgba(206, 206, 206, 0.4)',
                borderRadius: theme.sizes.s,
                overflow: 'auto',
                ...style,
            }}
        >
            <div style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingRight: theme.sizes.m,
                paddingLeft: theme.sizes.m,
                position: 'sticky',
                zIndex: 4,
                backgroundColor: theme.colors.background,
                top: 0,
            }}>
                <div style={{display: 'flex', alignItems: 'center', width: '100%'}}>
                    <div style={{
                        color: theme.colors.text,
                        ...theme.textSizes.subtitle2,
                        paddingTop: theme.sizes.m,
                        marginBottom: theme.sizes.m,
                    }}>
                        {title}
                    </div>
                    {asideTitle}
                </div>
                <div style={{
                    display: 'flex',
                    alignItems: 'center',
                }}>
                    {asideButton}
                    {
                        button &&
                        <CircleButton
                            icon={icon}
                            size="s"
                            loading={isButtonLoading}
                            onPress={() => onPress()}
                        />
                    }
                </div>
            </div>
            <div style={{
                height: '90%',
                paddingLeft: theme.sizes.m,
                paddingRight: theme.sizes.m,
                paddingBottom: theme.sizes.m,
            }}>
                {children}
            </div>
        </div>
    )
}