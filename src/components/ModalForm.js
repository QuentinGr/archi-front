import {useRef, useState, useImperativeHandle, forwardRef} from "react";
import {useForm} from "react-hook-form";
import {FormGroup, OverModal} from "./index";

export const ModalForm = forwardRef(({
                                         onSubmit,
                                         defaultForm,
                                         children,
                                         addTitle,
                                         modifyTitle,
                                         onIsAddingChange = () => {
                                         },
                                         loading = false,
                                     }, ref) => {
    const [isAdding, setIsAdding] = useState(true)

    const modalRef = useRef(null)

    const {control, handleSubmit, formState: {errors}, getValues, reset} = useForm({
        mode: 'onChange',
        reValidateMode: 'onChange',
        defaultValues: {
            defaultForm
        },
        criteriaMode: 'all',
    })

    useImperativeHandle(ref, () => {
        return {
            open: (adding, form) => openModal(adding, form),
            close: () => modalRef.current.close(),
            getValues,
        }
    })

    const openModal = (isAdding, form) => {
        if (isAdding) {
            setIsAdding(true)
            onIsAddingChange(true)
            reset(defaultForm)
        } else {
            setIsAdding(false)
            onIsAddingChange(false)
            reset(form)
        }
        modalRef.current.open()
    }


    const submit = (data) => {
        onSubmit(data)
    }

    return (
        <div>
            <OverModal
                ref={modalRef}
                title={isAdding ? addTitle : modifyTitle}
                onSubmit={handleSubmit(submit)}
                loading={loading}
            >
                <FormGroup
                    control={control}
                    errors={errors}
                >
                    {children}
                </FormGroup>
            </OverModal>
        </div>
    )
})