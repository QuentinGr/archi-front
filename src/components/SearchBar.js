import {theme} from "../constants/theme";
import {Icon} from "./Icon";

export const SearchBar = ({onChange, value, placeholder, style, ...props}) => {
    return (
        <div style={{
            display: 'flex',
            alignItems: 'center',
            boxShadow: '0px 10px 20px rgba(206, 206, 206, 0.4)',
            width: '100%',
            borderRadius: theme.sizes.xl,
            marginBottom: theme.sizes.m,
            backgroundColor: theme.colors.background,
            ...style
        }}>
            <Icon name="magnifyingglass" size={theme.sizes.m} color={theme.colors.disabled} style={{
                paddingLeft: theme.sizes.s
            }}/>
            <input
                onChange={onChange}
                value={value}
                placeholder={placeholder}
                style={{
                    color: theme.colors.text,
                    width: '92%',
                    paddingTop: theme.sizes.s,
                    paddingBottom: theme.sizes.s,
                    paddingLeft: theme.sizes.s,
                    border: 'none',
                    outline: 'none',
                    borderRadius: theme.sizes.xl,
                }}
                color={theme.colors.text}
                {...props}
            />
        </div>
    )
}