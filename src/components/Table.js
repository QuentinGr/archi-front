import {cloneElement, Children} from 'react';
import {theme} from "../constants/theme";
import {Loader} from "./Loader";

export const Table = ({head, body, style, isLoading}) => {
    return (
        <div style={{
            width: '100%',
            overflow: 'auto',
            border: 1,
            borderRadius: theme.sizes.l,
            ...style
        }}>
            {
                !isLoading &&
                <table style={{
                    width: '100%',
                    borderRadius: theme.sizes.l
                }}>
                    <thead>
                    {head}
                    </thead>
                    <tbody>
                    {body}
                    </tbody>
                </table>
            }
            {
                isLoading &&
                <Loader
                    color={theme.colors.primary}
                    style={{
                        marginTop: theme.sizes.m
                    }}
                    size="s"
                />
            }
        </div>
    )
}

export const Tr = ({children, isHeader = false, ...props}) => {
    return (
        <tr style={{
            color: theme.colors.text,
            borderRadius: theme.sizes.l,
            position: isHeader ? 'sticky' : 'relative',
            top: 0,
            zIndex: isHeader ? 2 : 1,
            ...theme.textSizes.body2,
        }}
            {...props}
        >
            {
                Children.map(children, child => child ? cloneElement(child, {style: {backgroundColor: isHeader ? theme.palette.grey.light1 : theme.colors.background}}) : null)
            }
        </tr>
    )
}

export const Td = ({children, isAction = false, onClick, pointer = false, style}) => {
    return (
        <td
            style={{
                minWidth: isAction ? 100 : 150,
                padding: theme.sizes.s,
                position: isAction ? 'sticky' : 'relative',
                left: 0,
                right: 0,
                zIndex: isAction ? 2 : 1,
                cursor: isAction || pointer ? 'pointer' : 'default',
                ...style
            }}
            onClick={onClick}
        >
            {children}
        </td>
    )
}