import {Icon} from "./Icon";
import {Loader} from "./Loader";

import {theme} from "../constants/theme";

export const ButtonContainer = ({
                                    color = 'primary',
                                    border = true,
                                    filled = true,
                                    shadowSize,
                                    onPress = () => {},
                                    children,
                                    loading=false,
                                    style,
                                    fullWidth = false,
                                }) => {
    const shadow = shadowSize ? theme.shadow[shadowSize] : {}

    const onClick = () => {
        if(!loading){
            onPress()
        }
    }

    return (
        <div style={{
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            width: 'fit-content'
        }}>
            <div
                onClick={() => onClick()}
                style={{width: fullWidth ? '100%' : 'auto'}}>
                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor: filled ? theme.colors[color] : null,
                        borderWidth: border ? theme.borderSizes.m : 0,
                        borderStyle: 'solid',
                        borderColor: theme.colors[color],
                        shadowColor: shadowSize ? theme.colors[color] : null,
                        width: fullWidth ? '100%' : 'fit-content',
                        cursor: 'pointer',
                        ...shadow,
                        ...style,
                    }}
                >
                    {children}
                </div>
            </div>
        </div>
    )
}

export const Button = ({
                           title,
                           color = 'primary',
                           size = 'm',
                           icon,
                           side = 'left',
                           filled = true,
                           border = true,
                           shadowSize,
                           fullWidth = false,
                           onPress,
                           loading = false,
                           style
                       }) => {

    const paddingHorizontal = {
        xs: theme.sizes.xs,
        s: theme.sizes.s,
        m: theme.sizes.m,
        l: theme.sizes.l,
        xl: theme.sizes.xl,
    }

    const marginIcon = {
        xs: theme.sizes.xs,
        s: theme.sizes.xs,
        m: theme.sizes.s,
        l: theme.sizes.s,
        xl: theme.sizes.m,
    }

    return (
        <ButtonContainer
            color={color}
            border={border}
            filled={filled}
            shadowSize={shadowSize}
            onPress={onPress}
            fullWidth={fullWidth}
            loading={loading}
            style={{
                paddingLeft: paddingHorizontal[size],
                paddingRight: paddingHorizontal[size],
                paddingTop: theme.sizes.xs,
                paddingBottom: theme.sizes.xs,
                borderRadius: theme.sizes.xs,
                ...style,
            }}
        >
            {icon && side === 'left' && !loading &&
                <div style={{marginRight: marginIcon[size], display: 'flex', alignItems: 'center'}}>
                    <Icon
                        name={icon}
                        size={theme.sizes[size]}
                        color={filled ? theme.colors.background : theme.colors[color]}
                    />
                </div>
            }
            {
                !loading &&
                <div style={{
                    fontSize: theme.sizes[size],
                    fontFamily: theme.fonts.bold,
                    color: filled ? theme.colors.background : theme.colors[color],
                    width: fullWidth ? '100%' : 'auto',
                    textAlign: 'center',
                }}>{title}</div>
            }
            {icon && side === 'right' && !loading &&
                <div style={{marginLeft: marginIcon[size], display: 'flex', alignItems: 'center'}}>
                    <Icon
                        name={icon}
                        size={theme.sizes[size]}
                        color={filled ? theme.colors.background : theme.colors[color]}
                    />
                </div>
            }
            {
                loading &&
                <Loader
                    style={{height: theme.sizes[size] + theme.sizes.xxs}}
                    size="xs"
                    color={filled ? theme.colors.background : theme.colors[color]}/>
            }
        </ButtonContainer>
    )
}


export const CircleButton = ({
                                 color = 'primary',
                                 size = 'm',
                                 icon,
                                 filled = true,
                                 border = true,
                                 shadowSize,
                                 style,
                                 loading = false,
                                 onPress
                             }) => {

    const padding = {
        xs: theme.sizes.xxs,
        s: theme.sizes.xxs,
        m: theme.sizes.xs,
        l: theme.sizes.s,
        xl: theme.sizes.s,
        xxl: theme.sizes.m,
    }

    return (
        <ButtonContainer
            color={color}
            border={border}
            filled={filled}
            shadowSize={shadowSize}
            onPress={onPress}
            style={{
                padding: padding[size],
                borderRadius: theme.sizes.xxl,
                ...style
            }}
            rounded
        >
            {
                !loading &&
                <Icon
                    name={icon}
                    size={theme.sizes[size]}
                    color={filled ? theme.colors.background : theme.colors[color]}
                />
            }
            {
                loading &&
                <Loader
                    style={{height: theme.sizes[size] + theme.sizes.xxs}}
                    size="xxs"
                    color={filled ? theme.colors.background : theme.colors[color]}/>
            }
        </ButtonContainer>
    )
}