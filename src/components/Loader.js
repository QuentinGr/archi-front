import {
    useRef,
    forwardRef, useState,
    useImperativeHandle,
    useEffect
} from "react";

import {theme} from "../constants/theme";

const Dot = forwardRef(({size, color, style}, ref) => {

    const [translation, setTranslation] = useState(0)

    const bounce = (start = false) => {
        setTranslation(start ? -5 : 0)
    }

    useImperativeHandle(ref, () => {
        return {
            bounce,
        }
    })

    return (
        <div
            style={{
                width: theme.sizes[size],
                height: theme.sizes[size],
                backgroundColor: color,
                borderRadius: 50,
                transform: `translateY(${translation}px)` ,
                transitionProperty: 'all',
                transitionDuration: '0.2s',

                ...style
            }}
        />
    )
})

export const Loader = ({size, color, style}) => {
    const dot1 = useRef(null)
    const dot2 = useRef(null)
    const dot3 = useRef(null)

    const dots = [dot1, dot2, dot3]

    const wait = (ms) => {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    useEffect(() => {
        const bouncer = async () => {
            let i = 0
            while (true) {
                if (i !== 0 && dots[(i - 1) % dots.length].current){
                    dots[(i - 1) % dots.length].current.bounce(false)
                }

                if(i % dots.length ===  0){
                    await wait(1000)
                }

                if(dots[i % dots.length].current){
                    dots[i % dots.length].current.bounce(true)
                }

                await wait(200)
                i += 1
            }
        }

        bouncer()
    }, [])

    return (
        <div style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            height: theme.sizes[size],
            width: '100%',
            ...style,
        }}>
            <Dot ref={dot1} size={size} color={color} style={{marginRight: theme.sizes[size]}}/>
            <Dot ref={dot2} size={size} color={color} style={{marginRight: theme.sizes[size]}}/>
            <Dot ref={dot3} size={size} color={color}/>
        </div>
    )
}