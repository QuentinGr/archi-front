import {theme} from "../constants/theme";

export const Dot = ({color, label = '', style}) => {
    return (
        <div
            style={{
                display: 'flex',
                alignItems: 'center',
                ...style,
            }}>
            {
                label &&
                <div style={{
                    ...theme.textSizes.body2,
                    color: theme.colors.text,
                    marginRight: theme.sizes.s,
                }}>
                    {label} :
                </div>
            }
            <div style={{
                width: theme.sizes.s,
                height: theme.sizes.s,
                backgroundColor: theme.colors[color],
                borderRadius: '50%'
            }}/>
        </div>
    )
}