import {Root} from "./Root";
import {router} from "./routes";

export {Root, router}