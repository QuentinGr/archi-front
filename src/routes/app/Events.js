import {useCallback, useEffect, useRef, useState} from "react";
import axios from "axios";
import dayjs from "dayjs";

import {Button, Icon, Table, Td, Tr} from "../../components";
import {theme} from "../../constants/theme";
import {EventModal} from "./EventModal";
import {useNavigate} from "react-router-dom";

export const Events = () => {
    const [isLoading, setIsLoading] = useState(false)
    const [isLoadingPost, setIsLoadingPost] = useState(false)
    const [isEditing, setIsEditing] = useState(false)
    const [editIndex, setEditIndex] = useState(0)
    const [events, setEvents] = useState([])

    const eventModalRef = useRef(null)

    const navigate = useNavigate()

    const getEvents = useCallback(async () => {
        setIsLoading(true)
        setEvents((await axios.get('http://localhost:8080/events')).data)
        setIsLoading(false)
    }, [])

    const onClick = (index) => {
        navigate(`/event/${events[index].id}`)
    }

    const onEditClick = (index) => {
        setEditIndex(index)
        setIsEditing(true)
        eventModalRef.current.open(false, {
            ...events[index],
            start: dayjs(events[index].start).format('YYYY-MM-DD'),
            end: dayjs(events[index].start).format('YYYY-MM-DD'),
        })
    }

    const onDeleteClick = async (index) => {
        await axios.delete(`http://localhost:8080/events/${events[index].id}`)
        setEvents(e => e.filter((_, i) => i !== index))
    }

    const onSubmit = async (data) => {
        setIsLoadingPost(true)
        if (!isEditing) {
            const response = (await axios.post('http://localhost:8080/events', data)).data
            setEvents(e => [response, ...e])
        }else{
            const response = (await axios.put(`http://localhost:8080/events/${events[editIndex].id}`, data)).data
            setEvents(e => e.map((event, index) => index === editIndex ? response : event))
        }
        setIsLoadingPost(false)
        eventModalRef.current.close()
    }

    const addEvent = () => {
        eventModalRef.current.open(true, {
            name: '',
            description: '',
            start: '',
            end: '',
        })
    }

    useEffect(() => {
        getEvents()
    }, [getEvents])

    return (
        <div style={{
            marginLeft: theme.sizes.s,
            marginRight: theme.sizes.s,
        }}>
            <EventModal
                ref={eventModalRef}
                onSubmit={onSubmit}
                isLoading={isLoadingPost}
            />
            <Button
                title="Ajouter un évènement"
                onPress={() => addEvent()}
                style={{marginTop: theme.sizes.s, marginBottom: theme.sizes.s}}
            />
            <Table
                style={{
                    height: '80%'
                }}
                isLoading={isLoading}
                head={
                    <Tr isHeader={true}>
                        <Td>
                            Nom
                        </Td>
                        <Td>
                            Description
                        </Td>
                        <Td>
                            Date de début
                        </Td>
                        <Td>
                            Date de fin
                        </Td>
                        <Td isAction={true}>
                            ACTIONS
                        </Td>
                    </Tr>
                }
                body={
                    events.map((event, index) =>
                        <Tr key={event.id}>
                            <Td onClick={() => onClick(index)} pointer={true}>
                                {event.name}
                            </Td>
                            <Td onClick={() => onClick(index)} pointer={true}>
                                {event.description}
                            </Td>
                            <Td onClick={() => onClick(index)} pointer={true}>
                                {event.start ? dayjs(event.start).format('DD/MM/YYYY') : ''}
                            </Td>
                            <Td onClick={() => onClick(index)} pointer={true}>
                                {event.start ? dayjs(event.end).format('DD/MM/YYYY') : ''}
                            </Td>
                            <Td isAction={true}>
                                <div style={{
                                    display: 'flex',
                                    justifyContent: 'space-around',
                                }}>
                                    <Icon name="pencil" size={theme.sizes.s} onClick={() => onEditClick(index)}/>
                                    <Icon name="trash" size={theme.sizes.s} onClick={() => onDeleteClick(index)}/>
                                </div>
                            </Td>
                        </Tr>
                    )
                }
            />
        </div>
    )
}