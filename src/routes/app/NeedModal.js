import {forwardRef, useImperativeHandle, useRef} from "react";
import {ModalForm, TextInputForm} from "../../components";

export const NeedModal = forwardRef(({loading, onSubmit}, ref) => {
    const modalRef = useRef(null)

    useImperativeHandle(ref, () => {
        return {
            open: (adding, form) => modalRef.current.open(adding, form),
            close: () => modalRef.current.close()
        }
    })

    return (
        <ModalForm
            ref={modalRef}
            loading={loading}
            defaultForm={{name: '', description: ''}}
            onSubmit={onSubmit}
            addTitle="Ajouter un besoin"
            modifyTitle="Supprimer un besoin"
        >
            <TextInputForm
                label="Nom"
                name="name"
                placeholder="Ballon"
                icon="number"
                rules={{
                    required: true,
                }}
            />
            <TextInputForm
                label="Description"
                name="description"
                placeholder="Budget 20 euros"
                icon="number"
                rules={{
                    required: true,
                }}
            />
        </ModalForm>
    )
})