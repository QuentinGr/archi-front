import {useCallback, useEffect, useRef, useState} from "react";
import {useParams} from "react-router-dom";
import axios from "axios";
import {theme} from "../../constants/theme";
import {Button, Icon, Table, Td, Tr} from "../../components";
import {NeedModal} from "./NeedModal";

export const EventDetail = () => {
    const [event, setEvent] = useState({})
    const [needs, setNeeds] = useState([])
    const [isLoading, setIsLoading] = useState(false)
    const [isLoadingPost, setIsLoadingPost] = useState(false)
    const [isEditing, setIsEditing] = useState(false)
    const [editIndex, setEditIndex] = useState(0)

    const needModalRef = useRef(null)

    const {id} = useParams()

    const getEvent = useCallback(async () => {
        const response = await axios.get(`http://localhost:8080/events/${id}`)
        setEvent(response.data)
    }, [id])

    const getNeeds = useCallback(async () => {
        setIsLoading(true)
        const response = await axios.get(`http://localhost:8080/needs/byEvent/${id}`)
        setNeeds(response.data)
        setIsLoading(false)
    }, [id])

    const onEditClick = (index) => {
        setEditIndex(index)
        setIsEditing(true)
        needModalRef.current.open(false, needs[index])
    }

    const onDeleteClick = async (index) => {
        await axios.delete(`http://localhost:8080/needs/${needs[index].id}`)
        setNeeds(n => n.filter((_, i) => i !== index))
    }

    const addEvent = () => {
        needModalRef.current.open(true, {
            name: '',
            description: '',
        })
    }

    const onSubmit = async (data) => {
        setIsLoadingPost(true)
        if (!isEditing) {
            const response = (await axios.post('http://localhost:8080/needs', {
                ...data,
                eventIdRequest: event.id,
            })).data
            setNeeds(e => [response, ...e])
        } else {
            const response = (await axios.put(`http://localhost:8080/needs/${needs[editIndex].id}`, data)).data
            setNeeds(e => e.map((need, index) => index === editIndex ? response : need))
        }
        setIsLoadingPost(false)
        needModalRef.current.close()
    }

    useEffect(() => {
        getEvent()
    }, [getEvent])

    useEffect(() => {
        getNeeds()
    }, [getNeeds])

    return (
        <div style={{
            paddingLeft: theme.sizes.m,
            paddingRight: theme.sizes.m,
        }}>
            <NeedModal
                ref={needModalRef}
                onSubmit={onSubmit}
                isLoading={isLoadingPost}
            />
            <div style={{
                flexDirection: 'column',
                justifyContent: 'center',
                textAlign: 'center',
            }}>
                <div style={{
                    ...theme.textSizes.headline,
                    color: theme.colors.text,
                }}>
                    {event.name}
                </div>
                <div style={{
                    ...theme.textSizes.subtitle2,
                    color: theme.colors.text,
                }}>
                    {event.description}
                </div>
            </div>
            <Button
                title="Ajouter un besoin"
                onPress={() => addEvent()}
                style={{
                    marginTop: theme.sizes.m,
                    marginBottom: theme.sizes.m,
                }}
            />
            <Table
                style={{
                    height: '80%'
                }}
                isLoading={isLoading}
                head={
                    <Tr isHeader={true}>
                        <Td>
                            Nom
                        </Td>
                        <Td>
                            Description
                        </Td>
                        <Td isAction={true}>
                            ACTIONS
                        </Td>
                    </Tr>
                }
                body={
                    needs.map((event, index) =>
                        <Tr key={event.id}>
                            <Td pointer={true}>
                                {event.name}
                            </Td>
                            <Td pointer={true}>
                                {event.description}
                            </Td>
                            <Td isAction={true}>
                                <div style={{
                                    display: 'flex',
                                    justifyContent: 'space-around',
                                }}>
                                    <Icon name="pencil" size={theme.sizes.s} onClick={() => onEditClick(index)}/>
                                    <Icon name="trash" size={theme.sizes.s} onClick={() => onDeleteClick(index)}/>
                                </div>
                            </Td>
                        </Tr>
                    )
                }
            />
        </div>
    )
}