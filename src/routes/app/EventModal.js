import {forwardRef, useImperativeHandle, useRef} from "react";
import {ModalForm, TextInputForm} from "../../components";

export const EventModal = forwardRef(({onSubmit, loading}, ref) => {
    const modalRef = useRef(null)

    useImperativeHandle(ref, () => {
        return {
            open: (adding, form) => modalRef.current.open(adding, form),
            close: () => modalRef.current.close()
        }
    })

    return(
        <ModalForm
            ref={modalRef}
            loading={loading}
            defaultForm={{name: '', description: '', start: '', end: ''}}
            onSubmit={onSubmit}
            addTitle="Ajouter un évènement"
            modifyTitle="Supprimer un évènement"
        >
            <TextInputForm
                label="Nom"
                name="name"
                placeholder="Match de foot"
                icon="number"
                rules={{
                    required: true,
                }}
            />
            <TextInputForm
                label="Description"
                name="description"
                placeholder="Prendre un ballon"
                icon="number"
                rules={{
                    required: true,
                }}
            />
            <TextInputForm
                label="Début"
                name="start"
                placeholder="Prendre un ballon"
                type="date"
                rules={{
                    required: true,
                }}
            />
            <TextInputForm
                label="Fin"
                name="end"
                placeholder="Prendre un ballon"
                type="date"
                rules={{
                    required: true,
                }}
            />
        </ModalForm>
    )
})