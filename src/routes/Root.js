import {Outlet} from "react-router-dom";


export const Root = () => {

    return (
        <div style={{
            height: '100vh',
            width: '100%',
        }}>
            <Outlet/>
        </div>
    )
}
