import {createBrowserRouter} from "react-router-dom";
import {Root} from "./index";
import {EventDetail, Events} from "./app";

export const router = createBrowserRouter([
    {
        path: "/",
        element: <Root/>,
        children: [
            {
                path: "events",
                element: <Events/>,
            },
            {
                path: "event/:id",
                element: <EventDetail/>,
            },
        ]
    },
]);