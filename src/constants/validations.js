export const validations = {
    email: v => {
        if (v){
            return !!v.toLowerCase().match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/)
        }
        return false
    },

    number: v => {
        if(v){
            return !!Number(v) && Math.ceil(Number(v)) === Number(v)
        }
        return false
    },

    decimal: v => {
        if(v){
            return !!Number(v)
        }
    }
}

export const validationMessages = {
    required: "Ce champ est requis",
    email: "Ce champ doit être un email",
    number: "Ce champ doit être un nombre entier",
    decimal: "Ce champ doit être un nombre décimal",
    min: "Ce champ à un minimum",
    minLength: "Ce champ à une taille minimum",
    usernameExist: "Ce nom d'utilisateur est déjà utilisé",
    emailExist: "Cet email est déjà utilisé",
    samePassword: 'Les mots de pas ne correspondent pas',
    differentThanUsername: "Mot de passe trop similaire au nom d'utilisateur",
    passwordLength: "Le mot de passe doit contenir au moins 8 caractères",
    minLengthDigit: 'Le mot de passe doit contenir au moins 1 chiffre',
    minLengthSpecial: 'Le mot de passe doit contenir au moins 1 caractère spécial',
    minLengthUpper: 'Le mot de passe doit contenir au moins 1 majuscule',
    minLengthChar: 'Le mot de passe doit contenir au moins 1 minuscule'
}