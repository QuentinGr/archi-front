export const palette = {
    red: {
        light1: "#FFE0E0",
        primary1: "#E10000",
    },
    green: {
        light1: "#D4F9E0",
        primary1: "#50AE6D",
    },
    orange: {
        light1: "#FFE9CE",
        primary1: "#ED805D",
    },
    sand: {
        light1: "#FFF0CD",
        primary1: "#FFBA7A",
    },
    cyan: {
        light1: "#E2F9FF",
        primary1: "#81E8FF",
    },
    blue: {
        light1: "#C2DEFF",
        primary1: "#3756A3",
    },
    grey: {
        light1: "#FBFBFB",
        primary1: "#CECECE",
    },
    mono: {
        white: "#FFFFFF",
        dark: "#000000",
    }
}

export const colors = {
    background: palette.mono.white,
    backgroundForCard: palette.grey.light1,
    foreground: palette.mono.dark,
    primary: palette.orange.primary1,
    secondary: palette.sand.primary1,
    text: palette.blue.primary1,
    textLight: palette.blue.light1,
    success: palette.green.primary1,
    danger: palette.red.primary1,
    warning: palette.orange.primary1,
    disabled: palette.grey.primary1,
}

export const fonts = {
    bold: "PoppinsBold",
    semiBold: "PoppinsSemiBold",
    regular: "PoppinsRegular",
}

export const sizes = {
    xxs: 4,
    xs: 8,
    s: 12,
    m: 16,
    l: 20,
    xl: 24,
    xxl: 40,
}

export const borderSizes = {
    s: 1,
    m: 2,
    l: 3,
}

export const textSizes = {
    headline: {
        fontFamily: fonts.bold,
        fontSize: sizes.xl,
    },
    subtitle1: {
        fontFamily: fonts.bold,
        fontSize: sizes.l,
    },
    subtitle2: {
        fontFamily: fonts.regular,
        fontSize: sizes.l,
    },
    body2: {
        fontFamily: fonts.regular,
        fontSize: sizes.m
    },
    body1: {
        fontFamily: fonts.regular,
        fontSize: sizes.s
    },
    caption: {
        fontFamily: fonts.regular,
        fontSize: sizes.xs,
    }
}

export const shadow = {
    s: {
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.5,
        shadowRadius: 10,
        elevation: 3
    },
    m: {
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.7,
        shadowRadius: 15,
    },
    l: {
        shadowOffset: {
            width: 0,
            height: 10,
        },
        shadowOpacity: 0.9,
        shadowRadius: 15,

    }
}

export const navBarHeight = 80

export const theme = {
    textSizes,
    sizes,
    fonts,
    colors,
    borderSizes,
    shadow,
    palette,
    navBarHeight,
}

